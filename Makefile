# How do these papers keep having the same kind of figure S2?  Uncanny.
panels = S2A S2B
s2out = $(addsuffix .fa,$(addprefix parsed/,$(panels)))
pdb = $(addsuffix .fa,$(addprefix from-pdb/3PIQ_,A B))

.SECONDARY:

all: $(s2out) $(pdb)

parsed/%.fa: from-paper/fig%.txt
	python scripts/parse_s2.py $^ $@

from-pdb/%.fa:
	python scripts/download_ncbi.py protein $* $@
